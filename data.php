<?
$url = 'https://api.mongolab.com/api/1/databases/fitts/collections/results/?';
$qs = ['apiKey'=> 'w9OpwNKmKUmT14Q3gRbY43D3ktfktpZr'];

if (isset($_GET['output'])):

	function translate_cond($group, $cond) {
		switch ($group) {
			case 2:
				$options = [1 => 'Red', 'Green', 'Black'];
				return $options[$cond];
			case 5:
				$options = [1 => 'Trackpad', 'Mouse'];
				return $options[$cond];
			case 4:
				$options = [1 => 'Trackpad', 'Tablet'];
				return $options[$cond];
			case 3:
				$options = [1 => 'Mac', 'PC'];
				return $options[$cond];
			case 1:
			default:
				return $cond;
		}
	}

	function translate_device($device) {
		$raw_version = strtok($device, ' ');
		$garbage = ltrim(strtok(';'), '(');
		if ($garbage == 'iPad') {
			$machine = $garbage.'; '.strtok('like');
			$garbage = strtok(')');
		}
		else {
			$machine = strtok(')');
		}
		$engine = strtok(' ');
		$engine_2 = strtok(')');
		$garbage = strtok('/');
		$browser_version = strtok(' ');
		if (trim($garbage) == 'Version')
			$browser = strtok('/');
		else {
			$browser = $garbage;
			if (trim($browser) == 'CriOS') {
				$mobile = strtok(' ');
				$browser = strtok('/');
			}
			else {
				$garbage = strtok('/');
			}
		}
		$browser_patch = strtok('');

		return compact('machine', 'browser', 'browser_version');
	}

	$group = $_GET['output'];

	ob_start(function($input) {
		switch ($_GET['type']) {
			case 'csv':
				header('Content-Type: application/csv; charset=utf-8');
				header("Content-Disposition: attachment; filename=fitts-group{$group}-results.csv");
				header('Pragma: no-cache');
				return strtr($input, "'", '"');
			break;

			case 'table':
				$output = "<!--\n\n $input \n\n-->\n\n";
				$output .= '<table border="1" cellspacing="0" cellpadding="5">';
					$lines = explode("\n", $input);
					foreach ($lines as $i => $line) {
						if (!$line) continue;
						$td = ($i == 0)? 'th':'td';
						$output .= '<tr>';

						$cells = explode("','", trim($line, "',"));
						foreach ($cells as $cell)
							$output .= "<$td>$cell</$td>";

						$output .= '</tr>';
					}
				$output .= '</table>';
				return $output;
			break;

			default:
				return 'Unrecognized type!';
		}
	});


	$fields = [
		['#', 'date', 'time', 'cond'],
		['ball 1','ball 2','ball 3','ball 4','ball 5','ball 6','ball 7','ball 8','ball 9'],
		($group == 2)? ['Details', 'Machine', 'Browser'] : ['Machine', 'Browser']
	];

	switch($group) {
		case 1:
		case 3:
		case 4:
		case 5:
			$fields[1] = [
				'Small Near',	'Small Medium',		'Small Far',
				'Medium Near',	'Medium Medium',	'Medium Far',
				'Big Near',		'Big Medium',		'Big Far'
			];
		break;

		case 2:
			$fields[1] = [
				'Small Near Dark',		'Small Medium Neutral',		'Small Far Light',
				'Medium Near Neutral',	'Medium Medium Light',		'Medium Far Dark',
				'Big Near Light',		'Big Medium Dark',			'Big Far Neutral'
			];
		break;
	}
	$fields = array_merge($fields[0], $fields[1], $fields[2]);

	$fields = array_map(function($value) { return "'$value'"; }, $fields);
	echo implode(',', $fields)."\n";

	$qs['q'] = json_encode(['group' => "$group"]);
	$qs['s'] = json_encode(['subject' => 1, 'timestamp' => 1]);
	$data = json_decode(file_get_contents($url.http_build_query($qs)), true);

	foreach($data as $subject) {
		$subject_data = [
			$subject['subject'],
			strtok($subject['timestamp'], 'T'),
			strtok('.'),
			translate_cond($group, $subject['condition'])
		];

		for($ball_num = 1; $ball_num <= 9; $ball_num++) {
			$ball = "ball-$ball_num";
			$subject_data[] = (isset($subject[$ball]))? $subject[$ball] : '--';
		}

		if ($group == 2) $subject_data[] = $subject['details'];

		$device = translate_device($subject['userAgent']);
		$subject_data[] = $device['machine'];
//		$subject_data[] = $subject['userAgent'];
		$subject_data[] = $device['browser'].' '.$device['browser_version'];

		echo implode(',', array_map(function($v) { return "'$v'"; }, $subject_data))."\n";
	}
	ob_end_flush();

else:
	$data = json_decode(file_get_contents($url.http_build_query($qs)), true);
	$groups = [];
	foreach ($data as $subject) {
		$group = $subject['group'];
		if (isset($groups[$group]))
			++$groups[$group];
		else
			$groups[$group] = 1;
	}
?>
	<h1>Data from the HCI Mini-Project 2: Fitts's Law</h1>

	<ul>
	<? foreach ($groups as $group => $subjects): ?>
		<li>
			Group <?=$group?>, <?=$subjects?> subject(s):
			<a href="?type=csv&output=<?=$group?>">CSV File</a>
			<a href="?type=table&output=<?=$group?>">Live table</a>
		</li>
	<? endforeach ?>
	</ul>
<? endif ?>