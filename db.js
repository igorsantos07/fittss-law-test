db = {
	url: 'https://api.mongolab.com/api/1/databases/fitts/collections/results/',
	key: 'w9OpwNKmKUmT14Q3gRbY43D3ktfktpZr',
	send: function(verb, data, oid, success) {
		var url = this.url
		if (oid) url = url+oid
		url += '?apiKey='+this.key

		$.ajax(url, { type: verb, contentType: 'application/json', data: JSON.stringify(data) })
			.success(function(data, status, xhr) {
				window.parent.oid = data._id.$oid
				if (success) success()
				else alert('Saved!')
			})
			.fail(function(xhr, status, error) {
				alert('Ops! '+error)
			})
	},
	save: function(data, success) {
		this.send('POST', data, success)
	},

	add: function(oid, data, success) {
		this.send('PUT', { '$set': data }, oid, success)
	}
}
