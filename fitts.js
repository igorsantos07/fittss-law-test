var ballData = {
	width: { s: 10, m: 20, l: 40 },
	distance: { s: 0.2, m: 0.4, l: 0.8 },
	color: {
		def: { white: 0, blue: 1, green: 2, red: 3, gold: 4 },
		special: {
			red: ['red-dark', 'red-bright', 'red-light'],
			green: ['green-dark', 'green-bright', 'green-light'],
			black: ['black-dark', 'black-bright', 'black-light']
		}
	}
}

var randomBalls = [
	{ width: ballData.width.s, distance: ballData.distance.s, color: ballData.color.def.blue },
	{ width: ballData.width.s, distance: ballData.distance.m, color: ballData.color.def.green },
	{ width: ballData.width.s, distance: ballData.distance.l, color: ballData.color.def.red },
	{ width: ballData.width.m, distance: ballData.distance.s, color: ballData.color.def.green },
	{ width: ballData.width.m, distance: ballData.distance.m, color: ballData.color.def.red },
	{ width: ballData.width.m, distance: ballData.distance.l, color: ballData.color.def.blue },
	{ width: ballData.width.l, distance: ballData.distance.s, color: ballData.color.def.red },
	{ width: ballData.width.l, distance: ballData.distance.m, color: ballData.color.def.blue },
	{ width: ballData.width.l, distance: ballData.distance.l, color: ballData.color.def.green }
]

var groupSets = [
	{ // Group J
		clicks: 6,
		maxBalls: 9,
		experimentNo: 4,
		multipleConditions: false,
		totalClicks: false,
		balls: randomBalls
	},

	{ // Group K
		clicks: 45,
		maxBalls: 9,
		experimentNo: 4,
		multipleConditions: true,
		totalClicks: true,
		balls: {
			1: [
				{ width: ballData.width.s, distance: ballData.distance.s, color: ballData.color.special.red[0] },
				{ width: ballData.width.s, distance: ballData.distance.m, color: ballData.color.special.red[1] },
				{ width: ballData.width.s, distance: ballData.distance.l, color: ballData.color.special.red[2] },
				{ width: ballData.width.m, distance: ballData.distance.s, color: ballData.color.special.red[1] },
				{ width: ballData.width.m, distance: ballData.distance.m, color: ballData.color.special.red[2] },
				{ width: ballData.width.m, distance: ballData.distance.l, color: ballData.color.special.red[0] },
				{ width: ballData.width.l, distance: ballData.distance.s, color: ballData.color.special.red[2] },
				{ width: ballData.width.l, distance: ballData.distance.m, color: ballData.color.special.red[0] },
				{ width: ballData.width.l, distance: ballData.distance.l, color: ballData.color.special.red[1] }
			],
			2: [
				{ width: ballData.width.s, distance: ballData.distance.s, color: ballData.color.special.green[0] },
				{ width: ballData.width.s, distance: ballData.distance.m, color: ballData.color.special.green[1] },
				{ width: ballData.width.s, distance: ballData.distance.l, color: ballData.color.special.green[2] },
				{ width: ballData.width.m, distance: ballData.distance.s, color: ballData.color.special.green[1] },
				{ width: ballData.width.m, distance: ballData.distance.m, color: ballData.color.special.green[2] },
				{ width: ballData.width.m, distance: ballData.distance.l, color: ballData.color.special.green[0] },
				{ width: ballData.width.l, distance: ballData.distance.s, color: ballData.color.special.green[2] },
				{ width: ballData.width.l, distance: ballData.distance.m, color: ballData.color.special.green[0] },
				{ width: ballData.width.l, distance: ballData.distance.l, color: ballData.color.special.green[1] }
			],
			3: [
				{ width: ballData.width.s, distance: ballData.distance.s, color: ballData.color.special.black[0] },
				{ width: ballData.width.s, distance: ballData.distance.m, color: ballData.color.special.black[1] },
				{ width: ballData.width.s, distance: ballData.distance.l, color: ballData.color.special.black[2] },
				{ width: ballData.width.m, distance: ballData.distance.s, color: ballData.color.special.black[1] },
				{ width: ballData.width.m, distance: ballData.distance.m, color: ballData.color.special.black[2] },
				{ width: ballData.width.m, distance: ballData.distance.l, color: ballData.color.special.black[0] },
				{ width: ballData.width.l, distance: ballData.distance.s, color: ballData.color.special.black[2] },
				{ width: ballData.width.l, distance: ballData.distance.m, color: ballData.color.special.black[0] },
				{ width: ballData.width.l, distance: ballData.distance.l, color: ballData.color.special.black[1] }
			]
		}
	},

	{ // Group L
		clicks: 6,
		maxBalls: 9,
		experimentNo: 4,
		multipleConditions: false,
		totalClicks: false,
		firstBall: { width: ballData.width.m, color: ballData.color.def.blue },
		balls: [
				{ width: ballData.width.s, distance: ballData.distance.s, color: ballData.color.special.red[1] },
				{ width: ballData.width.s, distance: ballData.distance.m, color: ballData.color.special.red[1] },
				{ width: ballData.width.s, distance: ballData.distance.l, color: ballData.color.special.red[1] },
				{ width: ballData.width.m, distance: ballData.distance.s, color: ballData.color.special.red[1] },
				{ width: ballData.width.m, distance: ballData.distance.m, color: ballData.color.special.red[1] },
				{ width: ballData.width.m, distance: ballData.distance.l, color: ballData.color.special.red[1] },
				{ width: ballData.width.l, distance: ballData.distance.s, color: ballData.color.special.red[1] },
				{ width: ballData.width.l, distance: ballData.distance.m, color: ballData.color.special.red[1] },
				{ width: ballData.width.l, distance: ballData.distance.l, color: ballData.color.special.red[1] }
			]
	},

	{ // Group M
		clicks: 6,
		maxBalls: 9,
		experimentNo: 4,
		multipleConditions: false,
		totalClicks: false,
		balls: randomBalls
	},

	{ // Group N
		clicks: 6,
		maxBalls: 9,
		experimentNo: 4,
		multipleConditions: false,
		totalClicks: false,
		balls: randomBalls
	}
]

var group = groupSets[window.parent.group - 1],
	setting = function(path, defValue) {
		var parts = path.split('.'),
			lastPart = group
		for (var i = 0; i < parts.length; i++) {
			lastPart = lastPart[parts[i]]
			if (lastPart === undefined)
				return defValue
		}
		return lastPart
	}

// config

/* should not be changed for now, since the background size is fixed */
var FieldWidth = 760.0;
var FieldHeight = 430.0;

/** Time needed to mark a coloured ball as "successfully hovered" for the pointing experiment */
var ParkTime = setting('parkTime', 750);

/** Pretty obvious. Number of clicks there should be in each ball to consider the experiment complete */
var ClicksPerBall = setting('clicks', 6);

/** WTF is this? */
var ClicksLastExperiment = 25;

/** If the objective is to point or click in the balls */
var Pointing = !!setting('pointing', false);

/** If it's needed to click in the white ball to start */
var NeedToClickOnStart = !!setting('needToClickOnStart', true);

/** If it's needed to click in the coloured ball to end */
var NeedToClickOnEnd = !!setting('needToClickOnEnd', true);

/** Maximum number of different balls in an experiment */
var MaxBalls = setting('maxBalls', 6);
parent.window.MaxBalls = MaxBalls;

/* Arrays where ball details will be stored */
var BallWidth = new Array(MaxBalls + 1);
var BallX = new Array(MaxBalls + 1);
var BallY = new Array(MaxBalls + 1);
/** The size of this array is equal to (biggest dimension / 10) +1. Example: we have 10, 20 and 40px balls, thus, (40/10)+1 */
var BallImages = new Array(4 + 1);

/**
 * Experiment - how will the balls' settings change?
 * 1 - same distance and size
 * 2 - different distance, same size
 * 3 - same distance, different size
 * 4 - different distance and size
 * @type Number
 */
var ExperimentNo = setting('experimentNo', 4);

var ShadowMargin = 10;

/** Always set to 1, don't know why. Looks related to the number of coloured balls at the same time in the screen */
var NBalls = setting('nballs', -1);

/** Number of different settings the experiment will have. Each phase usually is a different color ball, each one with different/equal settings of distance/size */
var NPhases = setting('multipleConditions')? (setting('balls.1').length || 1) : (setting('balls').length || 1);
var PhasesBall;
var PhaseBall;
var PhaseBallDetails;

/**
 * Mode is used to understand in which part of the experiment we are:
 * - 0 means "waiting for the white ball"
 * - 1 means "hovering the white ball"
 * - 2 means ???
 * - 3 means "waiting for the coloured ball"
 */
var Mode = -1;
var Phase = 1;

//var TimeSums = new Array(MaxBalls+1);

/** how many times each ball have already been clicked */
var Trials = new Array(MaxBalls + 1);


var curBall = -1;
var ParkTimeoutID;

// mode 0: outside / ended
// mode 1: inside the ball (parking)
// mode 2: inside the ball ready to run (parking complete)
// mode 3: running

// preloading
{
	BallImages[1] = new Array(5);
	BallImages[2] = new Array(5);
	BallImages[4] = new Array(5);
	PhasesBall = new Array(NPhases + 1);

	function loadFirstBall(withCond) {
		ballIndex = 2
		i = 0
		firstBall = setting('firstBall', { width: ballData.width.m, color: ballData.color.def.white })
		ballName = "ball"+firstBall.width+"-"+firstBall.color

		if (!withCond) {
			whiteBall = BallImages[ballIndex][i] = new Array(2)
			console.log('White ball:  ['+ballIndex+']['+i+'] = '+ballName)
		}
		else {
			whiteBall = BallImages[ballIndex][cond][i] = new Array(2)
			console.log('White ball:  ['+ballIndex+']['+cond+']['+i+'] = '+ballName)
		}
		whiteBall[0] = new Image();
		whiteBall[0].src = "intr/"+ballName+"-inactive.png";
		whiteBall[1] = new Image();
		whiteBall[1].src = "intr/"+ballName+".png";
	}

	if (!setting('multipleConditions')) {
		for (i = 1; i <= setting('balls').length; i++) {
			var ball = setting('balls.'+(i-1)),
				ballIndex = ball.width / 10,
				ballName = "ball"+ball.width+"-"+ball.color

			BallImages[ballIndex][i] = new Array(2)

			BallImages[ballIndex][i][0] = new Image();
			BallImages[ballIndex][i][0].src = "intr/"+ballName+"-inactive.png";
			BallImages[ballIndex][i][1] = new Image();
			BallImages[ballIndex][i][1].src = "intr/"+ballName+".png";

			PhasesBall[i] = BallImages[ballIndex][i]

			console.log('Ball loaded: ['+ballIndex+']['+i+'] = '+ballName)
		}
		loadFirstBall()
	}
	else {
		for(var cond in setting('balls')) {
			for (i = 1; i <= setting('balls.'+cond).length; i++) {
				var ball = setting('balls.'+cond+'.'+(i-1)),
					ballIndex = ball.width / 10,
					ballName = "ball"+ball.width+"-"+ball.color

				if (BallImages[ballIndex][cond] === undefined)
					BallImages[ballIndex][cond] = new Array()

				BallImages[ballIndex][cond][i] = new Array(2)

				BallImages[ballIndex][cond][i][0] = new Image();
				BallImages[ballIndex][cond][i][0].src = "intr/"+ballName+"-inactive.png";
				BallImages[ballIndex][cond][i][1] = new Image();
				BallImages[ballIndex][cond][i][1].src = "intr/"+ballName+".png";

				console.log('Ball loaded: ['+ballIndex+']['+cond+']['+i+'] = '+ballName)
			}
			loadFirstBall(true)
		}

		for(var cond in setting('balls')) {
			for (i = 1; i <= setting('balls.'+cond).length; i++) {
				var ball = setting('balls.'+cond+'.'+(i-1)),
					ballIndex = ball.width / 10,
					ballName = "ball"+ball.width+"-"+ball.color
				PhasesBall[i] = BallImages[ballIndex][parent.window.condition][i]
			}
		}
	}
}


var skipPhases = []
/** possibility to change the order in which the phases are shown */
function setPhase() {

	var gotPhase = false
	while (!gotPhase) {
		if (skipPhases.length == NPhases) {
			alert('The test is now over. Please click in the "Next" button.')
			Phase = Math.floor(Math.random() * NPhases + 1)
			gotPhase = true
		}
		Phase = Math.floor(Math.random() * NPhases + 1)


		if (!setting('totalClicks')) {
			//looking for other phase if this one got two more clicks than needed
			if (skipPhases.length > 0) {
				for (var i in skipPhases)
					if (skipPhases[i] == Phase)
						gotPhase = false
			}

			if (Trials[Phase] >= (ClicksPerBall + 2)) {
				gotPhase = false
				skipPhases[skipPhases.length] = Phase
			}
			else {
				gotPhase = true
			}
		}
		else {
			gotPhase = true
		}
	}

	PhaseBall = PhasesBall[Phase]

	if (!setting('multipleConditions')) {
		PhaseBallDetails = setting('balls')[Phase-1]
	}
	else {
		PhaseBallDetails = setting('balls')[window.parent.condition][Phase-1]
	}
	//Phase++;
	//if (Phase==NPhases+1) Phase=1;
	if (!setting('multipleConditions')) {
		console.log('Loading phase '+Phase+' out of '+NPhases+'. Used ball: '+PhaseBall[1].src)
	}
	else {
		console.log('Loading phase '+Phase+' out of '+NPhases+'. Used ball: '+PhaseBall[1].src)
	}
}

function startTimer() {
	timeStart = new Date().getTime();
}

function stopTimer(ballNo) {
	timeEnd = new Date().getTime();

	//TimeSums[ballNo]+=(timeEnd-timeStart);
	ballNo = Phase;

	//TODO this part talking about Ready and etc could be optimized
	if (setting('totalClicks')) {
		Trials++;
		console.log('Trials: '+Trials);

		Ready = 1;
		if (Trials < ClicksPerBall) {
			Ready = 0;
		}

		setPhase()

		if (!parent.window.ExperimentReady) {
			if (Ready) {
				parent.window.experimentReady()
			}
		}
	}
	else {
		Trials[ballNo]++;
		console.log(Trials);

		Ready = 1;
		for (i = 1; i <= NPhases; i++)
			if (Trials[i] < ClicksPerBall) {
				Ready = 0;
				break;
			}

		setPhase()

		if (!parent.window.ExperimentReady) {
			if (Ready) {
				parent.window.experimentReady()
			}
		}
	}

	parent.window.NResults[ballNo]++;
	parent.window.Results[ballNo][parent.window.NResults[ballNo]] = timeEnd - timeStart;
	if (ExperimentNo == 5) {
		parent.window.ResultsWidth[ballNo][parent.window.NResults[ballNo]] = BallWidth[1];
		parent.window.ResultsDistance[ballNo][parent.window.NResults[ballNo]] = Math.sqrt((BallX[1] - BallX[0]) * (BallX[1] - BallX[0]) + (BallY[1] - BallY[0]) * (BallY[1] - BallY[0]));
	}

	randomBall();
	prepareBallObjects(1);
}

function parkingTimer() {
	if (Mode == 1)
		changeMode(2);
}

function randomBall() {
	switch (ExperimentNo) {
		/*
		case 1:
			BallX[0] = Math.random() * (FieldWidth - 80) + 40;
			BallY[0] = Math.random() * (FieldHeight - 140) + 50;
			BallWidth[0] = 20;

			BallWidth[1] = 20;

			do {
				angle = Math.random() * 3.14;
				BallX[1] = BallX[0] + 0.333 * FieldWidth * Math.cos(angle);
				BallY[1] = BallY[0] + 0.333 * FieldWidth * Math.sin(angle);
			} while ((BallX[1] < 40) || (BallX[1] > (FieldWidth - 40)) || (BallY[1] < 50) || (BallY[1] > (FieldHeight - 90)));
		break;

		case 2:
			Okay = 0;

			while (!Okay) {
				BallX[0] = Math.random() * (FieldWidth - 80) + 40;
				BallY[0] = Math.random() * (FieldHeight - 140) + 50;
				BallWidth[0] = 20;

				BallWidth[1] = 20;

				Distance = 0.2 * Phase;

				Trial = 0;
				do {
					Trial++;
					angle = Math.random() * 3.14;
					BallX[1] = BallX[0] + Distance * FieldWidth * Math.cos(angle);
					BallY[1] = BallY[0] + Distance * FieldWidth * Math.sin(angle);

					if ((BallX[1] > 40) && (BallX[1] < (FieldWidth - 40)) && (BallY[1] > 50) && (BallY[1] < (FieldHeight - 90)))
						Okay = 1;
				} while ((Trial < 10) && (!Okay));
			}
		break;

		case 3:
			Okay = 0;

			while (!Okay) {
				BallX[0] = Math.random() * (FieldWidth - 80) + 40;
				BallY[0] = Math.random() * (FieldHeight - 140) + 50;
				BallWidth[0] = 20;

				if (Phase == 1)
					BallWidth[1] = 10;
				else if (Phase == 2)
					BallWidth[1] = 20;
				else
					BallWidth[1] = 40;

				Trial = 0;
				do {
					Trial++;
					angle = Math.random() * 3.14;
					BallX[1] = BallX[0] + 0.333 * FieldWidth * Math.cos(angle);
					BallY[1] = BallY[0] + 0.333 * FieldWidth * Math.sin(angle);

					if ((BallX[1] > 40) && (BallX[1] < (FieldWidth - 40)) && (BallY[1] > 50) && (BallY[1] < (FieldHeight - 90)))
						Okay = 1;
				} while ((Trial < 10) && (!Okay));
			}
		break;
		*/

		case 4:
			Okay = 0;

			runs = 0
			while (!Okay) {
				runs++
				BallX[0] = Math.random() * (FieldWidth - 80) + 40;
				BallY[0] = Math.random() * (FieldHeight - 140) + 50;
				BallWidth[0] = 20;

				BallWidth[1] = PhaseBallDetails['width'];
				Distance = PhaseBallDetails['distance'];

				Trial = 0;
				do {
					Trial++;
					angle = Math.random() * 3.14;
					BallX[1] = BallX[0] + Distance * FieldWidth * Math.cos(angle);
					BallY[1] = BallY[0] + Distance * FieldWidth * Math.sin(angle);

					Okay = (
						(BallX[1] > 40) &&
						(BallX[1] < (FieldWidth - 40)) &&
						(BallY[1] > 50) &&
						(BallY[1] < (FieldHeight - 90)))
				} while ((Trial < 10) && (!Okay));

				if (runs > 100)
					throw new Error('Gave up!!!!')
			}
		break;

		/*
		case 5:
			BallX[0] = Math.random() * (FieldWidth - 80) + 40;
			BallY[0] = Math.random() * (FieldHeight - 140) + 50;
			BallWidth[0] = 20;

			size = Math.random();
			if (size < 0.333)
				BallWidth[1] = 10;
			else if (size < 0.666)
				BallWidth[1] = 20;
			else
				BallWidth[1] = 40;

			do {
				BallX[1] = Math.random() * (FieldWidth - 80) + 40;
				BallY[1] = Math.random() * (FieldHeight - 140) + 50;
			} while ((BallX[1] > BallX[0] - 50) && (BallX[1] < BallX[0] + 50) && (BallY[1] > BallY[0] - 50) && (BallY[1] < BallY[0] + 50));
		break;
		*/

	   default:
		   console.log('Trying to run an inexistent experiment: '+ExperimentNo)
		break;
	}
}

function prepareBallObjects(again) {
	for (i = 0; i <= NBalls; i++) {
		if (again) {
			el = document.getElementById("ball" + i);
		} else {
			el = document.createElement("div");
			el.setAttribute("id", "ball" + i);
		}

		if (ExperimentNo == 5)
			No = Phase * 2 - 1;
		else
			No = Phase;

		BallImg = PhaseBall[1].src

		el.innerHTML = '<img style="filter:progid:DXImageTransform.Microsoft.Alpha(opacity=0); width: ' + parseInt(BallWidth[i] + ShadowMargin) + 'px; height: ' + parseInt(BallWidth[i] + ShadowMargin) + 'px;" id="ball' + i + 'image" src="' + BallImg + '">';
		el.style.zIndex = 1;
		el.style.left = parseInt(BallX[i] - (BallWidth[i] + ShadowMargin) / 2) + "px";
		el.style.top = parseInt(BallY[i] - (BallWidth[i] + ShadowMargin) / 2) + "px";
		el.style.width = parseInt(BallWidth[i] + ShadowMargin) + "px";
		el.style.height = parseInt(BallWidth[i] + ShadowMargin) + "px";
		el.style.position = 'absolute';
		el.style.border = '0px';
		el.style.filter = "progcid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + BallImg + "');";

		if (!again)
			document.body.appendChild(el);

		el.onclick = ballClickEvent;
		el.onmouseover = ballOverEvent;
		el.onmouseout = ballOutEvent;
		el.onmousemove = ballMoveEvent;
		el.ondragstart = ballDragStartEvent;

		if (again) {
			el = document.getElementById("ball" + i + "text");
		} else {
			el = document.createElement("span");
			el.setAttribute("id", "ball" + i + "text");
			el.className = "balldesc";
		}
		el.style.zIndex = 1;
		el.style.left = parseInt(BallX[i] - 50) + "px";
		el.style.top = parseInt(BallY[i] + (BallWidth[i] + ShadowMargin) / 2 - 5) + "px";
		el.style.width = "100px";
		el.style.height = "20px";
		el.style.position = 'absolute';
		el.style.border = '0px';
		if (Pointing)
			el.innerHTML = "point at me";
		else
			el.innerHTML = "click me";
		el.style.display = 'none';

		if (!again)
			document.body.appendChild(el);
	}
}

function changeMode(newMode) {
	if (Mode != newMode) {
		Mode = newMode;

		switch (Mode) {
			case 0:
			case 1:
				document.getElementById('ball0image').src = (setting('multipleConditions'))? BallImages[BallWidth[0] / 10][window.parent.condition][0][1].src : BallImages[BallWidth[0] / 10][0][1].src;
				document.getElementById('ball0').style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + document.getElementById('ball0image').src + "');";

				if (Pointing) {
					if (Mode == 0)
						document.getElementById('ball0text').innerHTML = "point at me";
					else
						document.getElementById('ball0text').innerHTML = "wait here";
				}
				document.getElementById('ball0text').style.display = 'block';

				for (i = 1; i <= NBalls; i++) {
					document.getElementById('ball' + i + 'image').src = PhaseBall[0].src;
					document.getElementById('ball' + i).style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + document.getElementById('ball' + i + 'image').src + "');";
					document.getElementById('ball' + i + 'text').style.display = 'none';
				}
				break;

			case 2:
			case 3:
				document.getElementById('ball0image').src = (setting('multipleConditions'))? BallImages[BallWidth[0] / 10][window.parent.condition][0][0].src : BallImages[BallWidth[0] / 10][0][0].src;
				document.getElementById('ball0').style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + document.getElementById('ball0image').src + "');";
				document.getElementById('ball0text').style.display = 'none';
				for (i = 1; i <= NBalls; i++) {
					document.getElementById('ball' + i + 'image').src = PhaseBall[1].src;
					document.getElementById('ball' + i).style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + document.getElementById('ball' + i + 'image').src + "');";
					document.getElementById('ball' + i + 'text').style.display = 'block';
				}
				if (document.getElementById("seconddevice"))
					document.getElementById("seconddevice").innerHTML = '';

				break;
		}

	}
}

function Start() {
	NBalls = 1;

//	if (ExperimentNo == 5)
//		Phase = 1;
//	else {
//		if (ExperimentNo == 4)
//			NPhases = 4;
//		else
//			NPhases = 3;
		setPhase()
//	}

	randomBall();

	if (setting('totalClicks')) {
		Trials = 0
		console.log('Calculating trials as a whole. Trials is a number, not an array.')
	}
	else {
		for (i = 0; i < Trials.length; i++) {
			//TimeSums[i]=0;
			Trials[i] = 0;
		}
		console.log('Total trials: '+(Trials.length))
		console.log(Trials)
	}

	prepareBallObjects(0);

	changeMode(0);
}

//-----------------------------------

function ballOver(ballNo) {
	if ((Mode == 0) && (ballNo == 0)) { // moved the mouse to park it
		changeMode(1);
		if (!NeedToClickOnStart)
			ParkTimeoutID = setTimeout("parkingTimer()", ParkTime);
	} else
	if ((Mode == 2) && (ballNo == 0)) {
		startTimer();
		changeMode(3);
	} else
	if ((Mode == 3) && (ballNo > 0) && (!NeedToClickOnEnd)) {
		stopTimer(ballNo);
		changeMode(0);
	}
}

function ballMove(ballNo) {
	if ((Mode == 1) && (ballNo == 0) && (!NeedToClickOnStart)) { // moved the mouse while parking
		clearTimeout(ParkTimeoutID);
		ParkTimeoutID = setTimeout("parkingTimer()", ParkTime);
		changeMode(1);
	}
}

function ballOut(ballNo) {
	if ((Mode == 1) && (ballNo == 0)) { // moved outside while parking
		changeMode(0);
		if (!NeedToClickOnStart)
			clearTimeout(ParkTimeoutID);
	} else
	if ((Mode == 2) && (ballNo == 0) && (!NeedToClickOnStart)) {
		startTimer();
		changeMode(3);
	}
}

function ballClick(ballNo) {
	if (curBall != ballNo)
		return;

	if ((Mode == 1) && (ballNo == 0) && (NeedToClickOnStart)) {
		startTimer();
		changeMode(3);
	} else
	if ((Mode == 3) && (ballNo > 0) && (NeedToClickOnEnd)) {
		stopTimer(ballNo);
		changeMode(0);
	}
}

//--------------------------------------------

function ballOutEvent(event) {
	if (curBall > -1) {
		ballOut(curBall);
		curBall = -1;
	}
}

function ballMoveEvent(event) {
	if (!event)
		var event = window.event;
	if (event.target)
		src = event.target;
	else if (event.srcElement)
		src = event.srcElement;
	ballNo = src.id.substring(4, 5);

	src = document.getElementById("ball" + ballNo);

	radius = parseInt(src.style.width) / 2;

	cx = parseInt(src.style.left) + radius;
	cy = parseInt(src.style.top) + radius;

	mx = event.clientX;
	my = event.clientY;

	dist = Math.sqrt((cx - mx) * (cx - mx) + (cy - my) * (cy - my));

	if (dist <= radius) {
		if (curBall > -1) {
			ballOut(curBall);
			ballOver(ballNo);
		} else {
			ballOver(ballNo);
		}
		curBall = ballNo;
	} else {
		if (curBall == ballNo) {
			ballOut(curBall);
			curBall = -1;
		}
	}

}

function ballOverEvent(event) {
	if (!event)
		event = window.event;

	ballMoveEvent(event);
}

function ballClickEvent(event) {
	if (!event)
		var event = window.event;
	if (event.target)
		src = event.target;
	else if (event.srcElement)
		src = event.srcElement;
	ballNo = src.id.substring(4, 5);

	ballClick(ballNo);
}

function ballDragStartEvent(event) {
	if (!event)
		var event = window.event;
	if (event.target)
		src = event.target;
	else if (event.srcElement)
		src = event.srcElement;
	ballNo = src.id.substring(4, 5);

	ballClick(ballNo);

	return(false);
}
